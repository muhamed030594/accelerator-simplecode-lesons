import 'dart:ui';

class AppColors {
  static const primary = Color(0xFF22A2BD);
  static const more1 = Color(0xFF43D049);
  static const more2 = Color(0xFFEB5757);
  static const neutral1 = Color.fromARGB(255, 185, 174, 174);
  static const neutral2 = Color(0xFF5B6975);
  static const neutral3 = Color.fromARGB(255, 59, 55, 55);
  static const mainText = Color(0xFF0B1E2D);
  static const splashBackground = Color(0xFF091824);
  static const background = Color(0xFFFFFFFF);
}
